// 座標と円の大きさの変数をつくる
float x, y, dia;
size(400, 400);
background(0);

// While文を用いた繰り返し操作
int i=0;
while(i<6){
  x = i*40;
  y = i*40;
  dia = 30;
  ellipse(x, y, dia, dia);
  i = i+1;
}