// 座標と円の大きさの変数をつくる
float x, y, dia;
size(400, 400);
background(0);

// for文を用いた繰り返し操作
for(int i=0; i<6; i=i+1){
  x = i*40;
  y = i*40;
  dia = 30;
  ellipse(x, y, dia, dia);
}