float x, y, dia;
color col; // 色の変数をつくる

size(400, 400);
background(0);

for (int i=1; i<20; i=i+1) {
  // ランダム関数でランダムな値を入れる。
  x = random(width);
  y = random(height);

  // 色の変数にランダムなRGBの値を入れる。
  col = color(random(255), random(255), random(255));
  fill(col);
  dia = random(10,80); // 10~80までのランダムな値
  ellipse(x, y, dia, dia);
}