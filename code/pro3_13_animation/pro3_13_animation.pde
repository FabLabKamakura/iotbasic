float x, y, dia;
color col; // 色の変数をつくる

void setup(){
  size(400, 400);
  background(0);
  frameRate(30); // 画面の更新頻度(frame per sec)
}

void draw(){
  // ランダム関数でランダムな値を入れる。
  x = random(width);
  y = random(height);

  // 色の変数にランダムなRGBの値を入れる。
  col = color(random(255), random(255), random(255));
  fill(col);
  dia = random(10,80); // 10~80までのランダムな値
  ellipse(x, y, dia, dia);
}