float x, y, dia;
color col; // 色の変数
int count; //draw実行回数をカウントする変数

void setup(){
  size(400, 400);
  background(0);
  frameRate(5); // 画面の更新頻度(frame per sec)
}

void draw(){
  x = random(width);
  y = random(height);
  col = color(random(255), random(255), random(255));
  fill(col);
  dia = random(10,80);
  count = count + 1;
  if(count >10){
    ellipse(x, y, dia, dia);
  } else {
    rect(x, y, dia, dia);
  }
}