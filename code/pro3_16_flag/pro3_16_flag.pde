float x, y, dia;
color col; // 色の変数
int count; //draw実行回数カウント
// フラグ( 楕円? or 矩形? )
boolean flag = false;

void setup(){
  size(400, 400);
  background(0);
  noStroke(); // 枠線なし
  frameRate(30); // 画面更新頻度
}

void draw(){
  x = random(width);
  y = random(height);
  
  // color(グレースケール)
  col =color(random(255),random(50,100)); 
  fill(col);
  dia = random(10,80);
  count = count + 1;
  if(count >30){
    flag = !flag;
    count = 0;
  }

  if( flag ){
    ellipse(x, y, dia, dia);
  } else {
    rect(x, y, dia, dia);
  }
}