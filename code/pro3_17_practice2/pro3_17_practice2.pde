float x, y, dia;
color col1, col2; // 色の変数
int count; //draw実行回数カウント
// フラグ( 楕円? or 矩形? )
boolean flag = false;

void setup(){
  size(400, 400);
  background(0);
  noStroke(); // 枠線なし
  frameRate(30); // 画面更新頻度
  dia = 60;
}

void draw(){
  x = random(width);
  y = random(height);


// color(グレースケール,透過度)
  col1 = color(255,255,0);
  col2 = color(0,0,255);
  
  count = count + 1;
  if(count >30){
    flag = !flag;
    count = 0;
  }

  if( flag ){
    fill(col1);
    ellipse(x, y, dia, dia);
  } else {
    fill(col2);
    ellipse(x, y, dia, dia);
  }
}