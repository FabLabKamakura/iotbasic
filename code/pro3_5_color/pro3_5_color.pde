size(400,400);  // 画面サイズ
// 色の指定
background(0);  // 背景色 ＝ 黒
fill(0,255,0);  // オブジェクトの色 ＝ 緑
stroke(0,0,255); // 線の色 ＝ 青
strokeWeight(1); // 線の太さ ＝ 1 pixel
// 円を描画
ellipse(width/2,height/2,200,200);