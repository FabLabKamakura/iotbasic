// 変数を使ってみましょう。
int x = width/2;
int y = height/2;
int dia = 100;

size(400, 400); 

// 色を指定
background(0); 
fill(255, 0, 0);  
stroke(0, 0, 255); 
strokeWeight(3);  

// 円を描画
ellipse(x, y, dia, dia);