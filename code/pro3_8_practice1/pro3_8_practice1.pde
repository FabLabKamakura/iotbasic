int line1_x1, line1_y1, line1_x2, line1_y2;
int line2_x1, line2_y1, line2_x2, line2_y2;
int circ_x, circ_y, dia;

void setup(){
  size(400,400);
  background(255);
  strokeWeight(10);
  
  line1_x1 = 0;
  line1_y1 = 0;
  line1_x2 = width;
  line1_y2 = height;
  line2_x1 = 0;
  line2_y1 = height;
  line2_x2 = width;
  line2_y2 = 0;
  circ_x = width/2;
  circ_y = height/2;
  dia = 200;
}

void draw(){
  stroke(0,0,255);
  line(line1_x1, line1_y1, line1_x2, line1_y2);
  line(line2_x1, line2_y1, line2_x2, line2_y2);
  
  fill(0,255,0);
  ellipse(circ_x, circ_y, dia, dia);
}