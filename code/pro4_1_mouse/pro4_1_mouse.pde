int red_x, red_y, red_h;
int blue_x, blue_y, blue_h;

void setup(){
  size(400,400);
  red_x = 0;
  red_y = 0;
  red_h = height/2;
  blue_x = 0;
  blue_y = red_h;
  blue_h = height;
}

void draw(){
  background(0);
  fill(255,0,0);
  rect(red_x,red_y, mouseX, red_h);
  fill(0,0,255);
  rect(blue_x, blue_y, mouseY, blue_h);
}