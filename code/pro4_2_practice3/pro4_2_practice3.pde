
int red_x, red_y, red_w;
int blue_x, blue_y, blue_w;

void setup(){
  size(400,400);
  red_x = 0;
  red_y = height;
  red_w = width/2;
  blue_x = width/2;
  blue_y = height;
  blue_w = width;
}

void draw(){
  background(0);
  fill(255,0,0);
  rect(red_x,red_y, red_w, -mouseX);
  fill(0,0,255);
  rect(blue_x, blue_y, blue_w, -mouseY);
}