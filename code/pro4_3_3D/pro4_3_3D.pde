void setup(){
  size(400, 400, P3D);
  stroke(100);
  frameRate(30);
}

void draw(){
  background(0);

  translate(width/2, height/2, -20);
  
  rotateX(mouseX/200.0); //x軸を基準に回転
  rotateY(mouseY/200.0); //Y軸を基準に回転
  
  box(width/2);
}