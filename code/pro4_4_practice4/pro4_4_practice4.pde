float gray, alpha;
color col;

void setup(){
  size(400, 400, P3D);
  stroke(100);
}

void draw(){
  background(0);

  translate(width/2, height/2, -20);
  rotateX(mouseX/200.0); //x軸を基準に回転
  rotateY(mouseY/200.0); //Y軸を基準に回転
  
  gray = map(mouseX, 0, width, 30, 255);
  alpha = map(mouseY, 0, height, 30, 255);
  col = color(gray, alpha);
  fill(col);
  
  box(width/2);
}