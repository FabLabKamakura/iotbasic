int state;

void setup(){
  size(400, 400);
  background(255);
  state=0;
}

void draw(){
  if(state==0){
    background(255);
  } else if(state==1){
    background(255,0,0);
  } else if(state==2){
    background(0,255,0);
  } else {
    background(0,0,255);
  }
}

void keyPressed(){
  state = state + 1;
  if(state>3){
    state=0;
  }
}