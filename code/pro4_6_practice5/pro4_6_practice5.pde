int state;
int x, y, dia;
color red, yellow, blue, white;

void setup(){
  size(400, 400);
  
  x = width/2;
  y = height/2;
  dia = 200;
  
  red = color(255,0,0);
  yellow = color(255,255,0);
  blue = color(0,0,255);
  white = color(255);
  
  background(white);
  noStroke();
  fill(red);
  ellipse(x, y, dia, dia);
  state=0;
}
void draw(){
  if(state==0){
    background(white);
    fill(red);
    ellipse(x, y, dia, dia);
  } else if(state==1){
    background(blue);
    fill(yellow);
    ellipse(x, y, dia, dia);
  } else if(state==2){
    background(yellow);
    fill(white);
    ellipse(x, y, dia, dia);
  } else {
    background(red);
    fill(blue);
    ellipse(x, y, dia, dia);
  }
}
void keyPressed(){
  state = state + 1;
  if(state>3){
    state=0;
  }
}